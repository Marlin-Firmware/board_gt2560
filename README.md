<h1 align="center">Marlin 3D Printer Firmware</h1>

This setup is for a single hotend nozzle **GT2560 v3** board with **TMC2208_STANDALONE** stepper drivers.

If you are using a single hotend nozzle with mixing extruder.
- #define MIXING_EXTRUDER

How to install.
-
- Install VS Code, then open the extensions and search "**_PlatformIO_**".
-  Click on the PlatformIO icon in the left-hand margin to reveal the “Quick Access” menu and select **_PIO Home -> Open_**.
- Select **_[Import Arduino Project]_** button, and select the Marlin firmware folder.
